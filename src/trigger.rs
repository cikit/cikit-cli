use log::{debug, trace};
use structopt::StructOpt;
use url::Url;

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
//#[snafu(visibility = "pub(crate)")]
pub enum TriggerError {
    #[snafu(display("This functionality is not yet implemented."))]
    NotImplementedError {},
    #[snafu(display("Error while interacting with Gitlab"))]
    GenericGitlabError { source: gitlab::Error },
}

type Result<T, E = TriggerError> = std::result::Result<T, E>;

#[derive(Debug, StructOpt)]
#[structopt(name = "provider", about = "Trigger Provider")]
pub enum TriggerProvider {
    #[structopt(name = "gitlab")]
    /// Trigger jobs in GitLab
    GitLab {
        #[structopt(long = "api-v4-url", env = "CI_API_V4_URL")]
        /// The API v4 endpoint of the GitLab instance the project to trigger is located
        api_v4_url: Url,
        #[structopt(long = "job-token", env = "CI_JOB_TOKEN")]
        /// Bucket to upload the artifact to
        job_token: String,
        #[structopt(long, default_value = "master")]
        /// Branch to trigger
        branch: String,
        #[structopt(long = "variables")]
        /// Variables to pass to the job triggered
        /// Each occurrence needs to have the  form VAR_NAME=value
        variables: Vec<String>,
        #[structopt()]
        /// Path of project to trigger
        project: String,
    },
}

/// Trigger another pipeline using the given provider.
pub fn run(provider: &TriggerProvider) -> Result<()> {
    debug!("{:?}", provider);
    match provider {
        TriggerProvider::GitLab {
            api_v4_url,
            job_token,
            project,
            branch,
            variables,
        } => {
            gitlab::trigger(&api_v4_url, &job_token, &project, &branch, &variables)
                .context(GenericGitlabError)?;
            Ok(())
        }
    }
}

pub mod gitlab {
    use log::trace;
    use serde_json::Value;
    use url::Url;

    use snafu::{ensure, ResultExt, Snafu};

    #[derive(Debug, Snafu)]
    #[snafu(visibility = "pub(crate)")]
    pub enum Error {
        #[snafu(display("The URL is invalid"))]
        InvalidUrlError { url: Url },
        #[snafu(display("Unable to generate Request"))]
        RequestBuildError { source: reqwest::Error },
        #[snafu(display("Unable execute Request"))]
        RequestExecutionError { source: reqwest::Error },
        #[snafu(display("API returned an error"))]
        APIError { status: reqwest::StatusCode },
        #[snafu(display("Variable parse error"))]
        VariableParseError { variable: String },
    }

    type Result<T, E = Error> = std::result::Result<T, E>;

    fn trigger_url(api_url: &Url, project: &str) -> Result<Url> {
        let mut url = api_url.clone();
        if let Some(mut path) = url.path_segments_mut().ok() {
            path.pop_if_empty()
                .push("projects")
                .push(project)
                .push("trigger")
                .push("pipeline");
        } else {
            return Err(Error::InvalidUrlError {
                url: api_url.clone(),
            });
        }
        Ok(url.clone())
    }

    fn trigger_request(
        url: &Url,
        token: &str,
        branch: &str,
        vars: Vec<(String, String)>,
    ) -> Result<reqwest::Request> {
        let form = reqwest::multipart::Form::new()
            .text("token", token.to_string())
            .text("ref", branch.to_string());
        let form = vars.into_iter().fold(form, |form, (k, v)| form.text(k, v));
        let client = reqwest::Client::new();
        let req = client
            .post(url.as_str())
            .multipart(form)
            .build()
            .context(RequestBuildError)?;
        trace!("Request: {:?}", req);
        Ok(req)
    }

    fn trigger_variables<'a, I>(variables: I) -> Result<Vec<(String, String)>>
    where
        I: IntoIterator<Item = &'a str>,
    {
        variables
            .into_iter()
            .map(|var| {
                let v: Vec<&str> = var.splitn(2, '=').collect();
                ensure!(
                    v.len() == 2,
                    VariableParseError {
                        variable: var.to_string()
                    }
                );
                Ok((format!("variables[{}]", v[0]), v[1].to_string()))
            })
            .collect()
    }

    pub fn trigger(
        url: &Url,
        token: &str,
        project: &str,
        branch: &str,
        variables: &[String],
    ) -> Result<Value> {
        let client = reqwest::Client::new();
        let url = trigger_url(url, project)?;
        let vars = trigger_variables(variables.iter().map(AsRef::as_ref))?;
        let req = trigger_request(&url, token, branch, vars)?;
        let mut res = dbg!(client.execute(req).context(RequestExecutionError))?;
        if !res.status().is_success() {
            return Err(Error::APIError {
                status: res.status(),
            });
        }
        let json = res.json().unwrap();
        trace!("Response Body: {:?}", json);
        Ok(json)
    }

    #[cfg(test)]
    mod tests {
        // Note this useful idiom: importing names from outer (for mod tests) scope.
        use super::*;

        #[test]
        fn test_url_with_1level_project() {
            let api_url = Url::parse("https://example.com/api/v4").unwrap();
            let url = trigger_url(&api_url, "myproject").unwrap();
            assert_eq!(
                "https://example.com/api/v4/projects/myproject/trigger/pipeline",
                url.as_str()
            );
        }
        #[test]
        fn test_url_with_2level_project() {
            let api_url = Url::parse("https://example.com/api/v4").unwrap();
            let url = trigger_url(&api_url, "mygroup/myproject").unwrap();
            assert_eq!(
                "https://example.com/api/v4/projects/mygroup%2Fmyproject/trigger/pipeline",
                url.as_str()
            );
        }
        #[test]
        fn test_url_with_byid_project() {
            let api_url = Url::parse("https://example.com/api/v4").unwrap();
            let url = trigger_url(&api_url, "345678").unwrap();
            assert_eq!(
                "https://example.com/api/v4/projects/345678/trigger/pipeline",
                url.as_str()
            );
        }

        #[test]
        fn test_url_with_trailing_slash() {
            let api_url = Url::parse("https://example.com/api/v4/").unwrap();
            let url = trigger_url(&api_url, "mine").unwrap();
            assert_eq!(
                "https://example.com/api/v4/projects/mine/trigger/pipeline",
                url.as_str()
            );
        }

        #[test]
        fn test_request() {
            let url =
                Url::parse("https://example.com/api/v4/projects/mine/trigger/pipeline").unwrap();
            let req = trigger_request(&url, "TOKEN", "master", vec![]).unwrap();
            assert_eq!("POST", req.method());
            assert_eq!(url.as_str(), req.url().as_str());

            let _body = req.body().unwrap();
            let _headers = req.headers();
        }

        #[test]
        fn test_variables() {
            let v = vec!["var1=adc", "var2=123", "var3=my=var"];
            let vt = trigger_variables(v).unwrap();
            assert_eq!(("variables[var1]".to_string(), "adc".to_string()), vt[0]);
            assert_eq!(("variables[var2]".to_string(), "123".to_string()), vt[1]);
            assert_eq!(("variables[var3]".to_string(), "my=var".to_string()), vt[2]);
        }

        #[test]
        fn test_invalid_variables() {
            let v = vec!["var1"];
            let err = trigger_variables(v);

            assert!(err.is_err(), "Variable should not be parsable");
        }
    }
}
