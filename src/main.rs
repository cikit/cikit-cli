use log::trace;
use snafu::{ResultExt, Snafu};
use std::ffi::OsStr;
use std::path::Path;
use structopt::StructOpt;

use cikit::artifacts;
use cikit::credential_helper;
use cikit::trigger;
use cikit::CiEnvironment;

#[derive(Debug, Snafu)]
//#[snafu(visibility = "pub(crate)")]
pub enum CiKitError {
    #[snafu(display("This functionality is not yet implemented."))]
    NotImplementedError {},
    #[snafu(display("Artifacts Error"))]
    ArtifactsError { source: artifacts::ArtifactsError },
    #[snafu(display("Trigger Error"))]
    TriggerError { source: trigger::TriggerError },
    #[snafu(display("Trigger Error"))]
    CredentialHelperError {
        source: credential_helper::CredentialHelperError,
    },
}

type Result<T, E = CiKitError> = std::result::Result<T, E>;

#[derive(Debug, StructOpt)]
struct CiKit {
    #[structopt(
        long = "environment",
        default_value = "auto",
        possible_values = &CiEnvironment::variants(),
        case_insensitive = true
    )]
    /// The environment in which CI Kit is running in.
    /// `auto` meaning CI Kit will try to automatically detect the environment
    environment: CiEnvironment,

    #[structopt(subcommand)]
    /// Subcommand to run
    cmd: CiKitCommand,
}

fn get_program_name() -> Option<String> {
    std::env::args()
        .next()
        .as_ref()
        .map(Path::new)
        .and_then(Path::file_name)
        .and_then(OsStr::to_str)
        .map(String::from)
}

impl CiKit {
    fn from_multicall_or_args() -> CiKit {
        match get_program_name() {
            Some(s) if s == "git-credential-ci" => CiKit {
                environment: CiEnvironment::Auto,
                cmd: CiKitCommand::CredentialHelper,
            },
            _ => CiKit::from_args(),
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "cikit", about = "CI Toolkit")]
#[allow(clippy::large_enum_variant)] // We don't care much about the enum size here as it is only constructed once
enum CiKitCommand {
    #[structopt(name = "begin")]
    /// Begin a new CI job.
    /// This should be called at the beginning of a CI job
    Begin {
        #[structopt(env = "CI_KIT_CAUSE")]
        /// The id(s) of the cause(s) that triggered the CI job
        cause: Vec<String>,
    },
    #[structopt(name = "end")]
    /// End the current CI job.
    /// This should be called at the end of a CI job
    End {},
    #[structopt(name = "artifacts")]
    /// Artifact related subtasks
    Artifacts(artifacts::ArtifactsProvider),
    #[structopt(name = "trigger")]
    /// Trigger related jobs
    Trigger(trigger::TriggerProvider),
    #[structopt(name = "credential-helper")]
    /// Run CI Kit as git credential helper
    CredentialHelper,
}

fn main() -> Result<(), CiKitError> {
    env_logger::init();
    openssl_probe::init_ssl_cert_env_vars();

    let opt = CiKit::from_multicall_or_args();

    trace!("{:?}", &opt);
    match opt.cmd {
        CiKitCommand::Begin { cause } => {
            trace!("{:?}", &cause);
            return Err(CiKitError::NotImplementedError {});
        }
        CiKitCommand::End {} => {
            return Err(CiKitError::NotImplementedError {});
        }
        CiKitCommand::Artifacts(provider) => artifacts::run(&provider).context(ArtifactsError)?,
        CiKitCommand::Trigger(provider) => trigger::run(&provider).context(TriggerError)?,
        CiKitCommand::CredentialHelper => {
            credential_helper::run().context(CredentialHelperError)?
        }
    }
    Ok(())
}
