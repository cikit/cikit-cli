use log::debug;

use super::ArtifactsError;
use super::{ArtifactHandler, DownloadResult, Result, UploadResult};
use std::io::{Read, Write};

pub use url::{ParseError, Url};

use mime::Mime;
use reqwest::header::{HeaderMap, HeaderName, HeaderValue, USER_AGENT};
use reqwest::Client;

use snafu::Snafu;

#[derive(Debug, Snafu)]
//#[snafu(visibility = "pub(crate)")]
pub enum ArtifactoryError {
    #[snafu(display("Unable to parse URL"))]
    UrlError { source: url::ParseError },
}

#[derive(Debug)]
pub struct Artifacts {
    server: Url,
    repository: String,
    api_key: String,
    client: Client,
}

impl Artifacts {
    pub fn new(repository: &str, server: &Url) -> Self {
        let api_key = std::env::var("ARTIFACTORY_API_KEY").expect("ARTIFACTORY_API_KEY not set");
        let client = Client::new();
        Artifacts {
            server: server.clone(),
            repository: repository.into(),
            api_key,
            client,
        }
    }

    fn file_url(&self, file: &str) -> Result<Url, ParseError> {
        Ok(self
            .server
            .join(&format!("{}/", self.repository))?
            .join(file)?)
    }

    fn http_headers(&self) -> HeaderMap {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static("cikit"));
        headers.insert(
            HeaderName::from_static("x-jfrog-art-api"),
            HeaderValue::from_str(&self.api_key).unwrap(),
        );
        //TODO: Upload with checksums https://stackoverflow.com/questions/40009688/artifactory-upload-with-checksum
        headers
    }
}

impl ArtifactHandler for Artifacts {
    fn upload(&self, mut content: Box<dyn Read>, name: &str, _mime: &Mime) -> UploadResult<()> {
        // TODO: This copies everything to ram which might fail for big objects.
        // We might need to find a way on how to do this more efficiently in reqwest
        // TODO: check if mimetype can be attached to file
        let mut buf = Vec::new();
        content.read_to_end(&mut buf).unwrap();
        let url = self.file_url(name).unwrap();
        debug!("Uploading to: {}", &url);
        let res = self
            .client
            .put(&url.to_string())
            .headers(self.http_headers())
            .body(buf)
            .send()
            .unwrap();
        if res.status().is_success() {
            Ok(())
        } else {
            Err(ArtifactsError::UploadServerError {
                code: u32::from(res.status().as_u16()),
            })
        }
    }

    fn download(&self, name: &str, mut content: Box<dyn Write>) -> DownloadResult<()> {
        let url = self.file_url(name).unwrap();
        debug!("Downloading from: {}", &url);
        let mut res = self
            .client
            .get(&url.to_string())
            .headers(self.http_headers())
            .send()
            .unwrap();
        if res.status().is_success() {
            res.copy_to(&mut content).unwrap();
            Ok(())
        } else {
            Err(ArtifactsError::DownloadServerError {
                code: res.status().as_u16() as u32,
            })
        }
    }
}
