use log::debug;

use super::{ArtifactHandler, DownloadResult, Result, UploadResult};
use super::{ContentReadError, ContentWriteError, DownloadServerError, UploadServerError};
use snafu::{ResultExt, Snafu};
use std::io::{Read, Write};

use mime::Mime;

use s3::bucket::Bucket;
use s3::credentials::Credentials;

// These are used in the public API of this module
use s3;
pub use s3::region::Region;
pub use url::Url;

#[derive(Debug, Snafu)]
pub enum S3Error {
    #[snafu(display("S3 operation failed"))]
    S3OperationError { source: s3::error::S3Error },
    #[snafu(display("Invalid S3 configuration for bucket: {}", bucket))]
    S3ConfigurationError {
        source: s3::error::S3Error,
        bucket: String,
    },
}

pub enum Endpoint {
    Region(Region),
    Url(Url),
}

#[derive(Debug)]
pub struct Artifacts {
    bucket: Bucket,
}

impl Artifacts {
    pub fn new(bucket: &str, endpoint: Endpoint) -> Result<Self> {
        let region = match endpoint {
            Endpoint::Region(region) => region,
            Endpoint::Url(url) => {
                // HACK: rust-s3 doesn't work if the endpoint ends with a /
                // But the URL crate always appends a / to the host so we need to remove it.
                // The proper thing to do is to fix rust-s3 to work with URLs too.
                let mut url = url.to_string();
                url.pop();

                Region::Custom {
                    endpoint: url,
                    region: "custom".into(),
                }
            }
        };
        let credentials = Credentials::default();
        let bucket = Bucket::new(bucket, region, credentials)
            .context(S3ConfigurationError { bucket })
            .context(super::GenericS3Error)?;
        Ok(Artifacts { bucket })
    }
}

impl ArtifactHandler for Artifacts {
    fn upload(&self, mut content: Box<dyn Read>, name: &str, mime: &Mime) -> UploadResult<()> {
        // TODO: This copies everything to ram which might fail for big objects.
        // We might need to switch to another S3 library to support stream uploading.
        let mut buf = Vec::new();
        content.read_to_end(&mut buf).context(ContentReadError)?;
        debug!("Uploading to: {:?} {} as {}", &self.bucket, &name, &mime);
        let (_, code) = self
            .bucket
            .put_object(name, &buf, &mime.to_string())
            .context(S3OperationError)
            .context(super::GenericS3Error)?;
        if code == 200 {
            Ok(())
        } else {
            UploadServerError { code }.fail()
        }
    }

    fn download(&self, name: &str, mut content: Box<dyn Write>) -> DownloadResult<()> {
        // TODO: This copies everything to ram which might fail for big objects.
        // We might need to switch to another S3 library to support stream uploading.
        debug!("Downloading from: {:?} {}", &self.bucket, &name);
        let (data, code) = self
            .bucket
            .get_object(name)
            .context(S3OperationError)
            .context(super::GenericS3Error)?;
        content.write_all(&data).context(ContentWriteError)?;
        if code == 200 {
            Ok(())
        } else {
            DownloadServerError { code }.fail()
        }
    }
}
