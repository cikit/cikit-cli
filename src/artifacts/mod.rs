use log::{debug, trace};
use mime::Mime;
use mime_guess;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use structopt::StructOpt;
use url::Url;

use snafu::Snafu;

#[derive(Debug, Snafu)]
//#[snafu(visibility = "pub(crate)")]
pub enum ArtifactsError {
    #[snafu(display("This functionality is not yet implemented."))]
    NotImplementedError {},
    #[snafu(display("Could not upload artifact"))]
    UploadServerError { code: u32 },
    #[snafu(display("Could not download artifact"))]
    DownloadServerError { code: u32 },
    #[snafu(display("Unable to read content"))]
    ContentReadError { source: std::io::Error },
    #[snafu(display("Unable to write content"))]
    ContentWriteError { source: std::io::Error },
    #[snafu(display("Error while interacting with Artifactory"))]
    GenericArtifactoryError {
        source: artifactory::ArtifactoryError,
    },
    #[snafu(display("Error while interacting with S3"))]
    GenericS3Error { source: s3::S3Error },
}

type UploadResult<T, E = ArtifactsError> = std::result::Result<T, E>;
type DownloadResult<T, E = ArtifactsError> = std::result::Result<T, E>;
type Result<T, E = ArtifactsError> = std::result::Result<T, E>;

#[derive(Debug, StructOpt)]
#[structopt(name = "provider", about = "Artifact Provider")]
pub enum ArtifactsProvider {
    #[structopt(name = "s3")]
    /// Download an artifact from a previous pipeline step
    S3 {
        #[structopt(
            long = "region",
            env = "AWS_DEFAULT_REGION",
            required_unless = "endpoint"
        )]
        /// Region to upload the artifact to
        region: Option<s3::Region>,
        #[structopt(long = "endpoint", required_unless = "region")]
        /// Endpoint URL to upload the artifact to
        endpoint: Option<Url>,
        #[structopt(long = "bucket")]
        /// Bucket to upload the artifact to
        bucket: String,
        #[structopt(subcommand)]
        /// Provider specific commands
        cmd: ArtifactsCommand,
        // TODO: Get environment credentials via structopt instead of relying on rust-s3 for better error messages
    },
    #[structopt(name = "artifactory")]
    /// Upload an artifact from the current pipeline step
    Artifactory {
        #[structopt(long = "server", env = "ARTIFACTORY_SERVER")]
        /// Artifactory URL upload the artifact to
        server: Url,
        #[structopt(long = "repository", env = "ARTIFACTORY_REPOSITORY")]
        /// Name of the artifactory repository
        repository: String,
        #[structopt(subcommand)]
        /// Provider specific commands
        cmd: ArtifactsCommand,
    },
}

#[derive(Debug, StructOpt)]
#[structopt(name = "artifacts", about = "Artifact Management")]
pub enum ArtifactsCommand {
    #[structopt(name = "download")]
    /// Download an artifact from a previous pipeline step
    Download {
        #[structopt()]
        /// Source on the artifact storage
        src: String,
        #[structopt(parse(from_os_str))]
        /// Path where the pipeline artifacts are extracted to.
        path: PathBuf,
    },
    #[structopt(name = "upload")]
    /// Upload an artifact from the current pipeline step
    Upload {
        #[structopt(parse(from_os_str))]
        /// Path to upload for the next pipeline step
        path: PathBuf,
        #[structopt()]
        /// Destination on artifact storage
        dest: String,
    },
}

pub fn run(provider: &ArtifactsProvider) -> Result<()> {
    debug!("{:?}", provider);
    let (provider, cmd) = match provider {
        ArtifactsProvider::S3 {
            bucket,
            region,
            endpoint,
            cmd,
        } => {
            let endpoint = if let Some(url) = endpoint {
                s3::Endpoint::Url(url.clone())
            } else if let Some(region) = region {
                s3::Endpoint::Region(region.clone())
            } else {
                // This should never happen as structop prevents this.
                // But the type system needs a default so we provide one
                s3::Endpoint::Region(s3::Region::EuCentral1)
            };

            let client = Box::new(s3::Artifacts::new(&bucket, endpoint)?);
            let manager = ArtifactManager::new(client);
            (manager, cmd)
        }
        ArtifactsProvider::Artifactory {
            repository,
            server,
            cmd,
        } => {
            let client = Box::new(artifactory::Artifacts::new(&repository, server));
            let manager = ArtifactManager::new(client);
            (manager, cmd)
        }
    };
    trace!("{:?}", (&provider, &cmd));
    match cmd {
        ArtifactsCommand::Upload { path, dest } => provider.upload_file(&path, &dest),
        ArtifactsCommand::Download { src, path } => provider.download_file(&src, &path),
    }
}

pub trait ArtifactHandler: core::fmt::Debug {
    fn upload(&self, content: Box<dyn Read>, name: &str, mime: &Mime) -> UploadResult<()>;
    fn download(&self, name: &str, content: Box<dyn Write>) -> DownloadResult<()>;
}

#[derive(Debug)]
pub struct ArtifactManager {
    client: Box<dyn ArtifactHandler>,
}

impl ArtifactManager {
    pub fn new(client: Box<dyn ArtifactHandler>) -> Self {
        ArtifactManager { client }
    }

    pub fn upload_file(&self, file: &Path, dest: &str) -> Result<()> {
        let mime = mime_guess::from_path(file).first_or_octet_stream();
        let content = Box::new(File::open(file).unwrap());
        self.client.upload(content, &dest, &mime).unwrap();
        Ok(())
    }

    pub fn download_file(&self, src: &str, file: &Path) -> Result<()> {
        let content = Box::new(File::create(file).unwrap());
        self.client.download(src, content).unwrap();
        Ok(())
    }
}

pub mod artifactory;
pub mod s3;
