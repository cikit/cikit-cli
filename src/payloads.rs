use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct JobBegin {
    project_token: String,
    job_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JobEnd {
    project_token: String,
    job_id: String,
}
