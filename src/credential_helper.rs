use git_credential::{Error, GitCredential};
use log::{debug, trace};
use std::env;

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
//#[snafu(visibility = "pub(crate)")]
pub enum CredentialHelperError {
    #[snafu(display("Error reading from stdin"))]
    InputError { source: Error },
    #[snafu(display("Error writing to stdout"))]
    OutputError { source: Error },
}

type Result<T, E = CredentialHelperError> = std::result::Result<T, E>;

pub fn run() -> Result<()> {
    let mut gc = GitCredential::from_reader(std::io::stdin()).context(InputError)?;

    trace!("Input received: {:?}", &gc);

    // Check if the Gitlab CI variables are set and use them if available
    if let Ok(host) = env::var("CI_SERVER_HOST") {
        debug!("Inside Gitlab CI with host: {}", &host);
        // Check if the request is for the CI server
        if Some(host) == gc.host {
            debug!("Gitlab CI CI_SERVER_HOST and git requested host match");
            // We are inside gitlab CI and the host matches the CI server
            gc.username = Some("gitlab-ci-token".into());
            gc.password = env::var("CI_JOB_TOKEN").ok();
        }
    }

    // The explicit variables GIT_USER, GIT_PASS take precedence over the CI provided once
    // but only if GIT_HOST also matches GIT_HOST
    if let Ok(host) = env::var("GIT_HOST") {
        debug!("GIT_HOST is set to host: {}", &host);
        if Some(host) == gc.host {
            debug!("GIT_HOST and git requested host match");
            // GIT_HOST matches, set the variables accordingly
            gc.username = env::var("GIT_USER").ok();
            gc.password = env::var("GIT_PASS").ok();
        }
    }

    trace!("Output produced: {:?}", &gc);

    let out = std::io::stdout();

    gc.to_writer(out).context(OutputError)?;

    Ok(())
}
