use structopt::clap::arg_enum;

arg_enum! {
    #[derive(Debug)]
    pub enum CiEnvironment {
      Auto,
      GitLabCI,
    }
}

pub mod artifacts;
pub mod credential_helper;
pub mod payloads;
pub mod trigger;
