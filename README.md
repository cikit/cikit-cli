# CI Kit

CI Kit simplifies common step that are commonly needed in CI pipelines.
It provides a command line utility with several sub commands that can be run from any CI environment.

## Usage

### Pipeline Triggering

Trigger other jobs from within a pipeline.

#### Gitlab

To trigger another job within a GitLab instance use:

```
cikit trigger gitlab <project>
```

`<project>` can either be the numeric project id, the path to the project in URL encoded form, e.g. `mygroup%2Fmyproject`.

CI Kit will automatically use unvironment variables to find the proper values for:

- The URL of the GitLab instance via `CI_API_V4_URL`
- The token to use via `CI_JOB_TOKEN`

All of these an be overridden using the corresponding command line flags.

By default the targets master branch is triggered. Use `--branch <branch>` to trigger a different one.

Additional variables can be passed via the `--variables <NAME=VALUE>` flag and will be available as environment variables during the build.

Examples usage:

```
cikit trigger gitlab 1235
cikit trigger gitlab mygroup%2Fmyproject --branch testing

# A full example
cikit trigger gitlab --api-v4-url http://localhost:8000/api/v4 --job-token 1234 --variables BUILD_ALL=1 --variables RUN_TESTS=1 myproject
```

### Artifact management

Artifact management can be done via the `cikit artifacts` subcommand.

It provides two methods `cikit artifacts <provider> upload` and `cikit artifacts <provider> download`.

#### Uploading

Uploading a local file to a remote artifact storage can be done with the `upload` command:

```
cikit artifacts <provider> upload <local file> <remote path>
```

An example uploading to an S3 compliant storage would be:

```
cikit artifacts s3 --region eu-central1 upload myfile.tar.gz my-file_1.2.3.tar.gz
```

#### Downloading

Downloading a local file from remote artifact storage can be done with the `download` command:

```
cikit artifacts <provider> download <remote path> <local file>
```

An example downloading from an S3 compliant storage would be:

```
cikit artifacts s3 --region eu-central1 download my-file_1.2.3.tar.gz myfile.tar.gz
```

#### Aritfact providers

CI Kit supports multiple artifact providers

##### S3 Compatible

The S3 provider expects the credentials to be available in the typical AWS variables:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_SESSION_TOKEN`

Further it needs to know what reagion to interact with.
This can be either specified on to command line via the `--region` parameter or via the `AWS_DEFAULT_REGION` environment variable.

It also needs to know the name of the bucket to unteract with, which can be specified using the `--bucket` parameter.

##### Artifactory

The Artifactory provider expects the an API Key to be available in following environment variable:

- `ARTIFACTORY_API_KEY`

It also needs to know what repository to interact with, which can be specified using the `--repository` parameter.

#### Usage examples

Upload and download from an artifactory server:

```
echo "Hello" > test-file.txt
export ARTIFACTORY_API_KEY="top-secret"
cikit artifacts artifactory --server http://artifactory.example.com --repository test-repo upload test-file.txt test-file_1.0.txt
cikit artifacts artifactory --server http://artifactory.example.com --repository test-repo download test-file_1.0.txt test-file-downloaded.txt
```

Upload and download to AWS eu-central-1:

```
echo "Hello" > test-file.txt
export AWS_ACCESS_KEY_ID="my-id"
export AWS_SECRET_ACCESS_KEY="my-key"
cikit artifacts s3 --region eu-central-1 --bucket test-bucket upload test-file.txt test-file_1.0.txt
cikit artifacts s3 --region eu-central-1 --bucket test-bucket download test-file_1.0.txt test-file-downloaded.txt
```

Upload and download to a custom S3 compliant minio instance:

```
echo "Hello" > test-file.txt
export AWS_ACCESS_KEY_ID="minio"
export AWS_SECRET_ACCESS_KEY="minio123"
cikit artifacts s3 --endpoint http://localhost:9000 --bucket test-bucket upload test-file.txt test-file_1.0.txt
cikit artifacts s3 --endpoint http://localhost:9000 --bucket test-bucket download test-file_1.0.txt test-file-downloaded.txt
```

### Git credential helper

CI Kit can be used as a git credential helper that allows git to receive credentials from it's environment.

#### Setup

Create a symlink named `git-credential-ci` pointing to the `cikit` binary to a location that is in your `PATH` (e.g. `/usr/local/bin`).

The configure git to use the credential helper: `git config --global credential.helper ci`

For more information on how to setup credential helpers see https://git-scm.com/docs/gitcredentials.

#### Gitlab CI

The credential helper will extract the credentials from the

- Host: `CI_SERVER_HOST` to match the host
- Password: `CI_JOB_TOKEN` to extract the token
- Username: The username is always set to `gitlab-ci-token` if `CI_SERVER_HOST` is present.

#### Explicit via environment variables

The credentials can also be explicitly set via environment variables.
These have the highest priority override any automatically detected values.

- Host: `GIT_HOST` must match with the host git wants to connect
- Password: `GIT_PASS`
- Username: `GIT_USER`


## Features

- [ ] Artifact Management
  - [ ] Artifcat upload at the end of a job
  - [ ] Artifact download at the beginning of a job
  - [x] Upload/Download a single file
  - [ ] Upload/Download a directory recursively
  - [ ] Support multiple artifact sets via artifact-id
  - Support multiple backends
    - [x] S3
    - [x] Artifactory
- [ ] Status reporting
  - [ ] Report start of job to CI Kit Dashboard
  - [ ] Report end of job to CI Kit Dashboard
  - [ ] Report artifact upload including URL
- [ ] Trigger follow up pipelines in different projects
  - [ ] Support multiple CI systems
    - [ ] Gitlab CI
    - [ ] Jenkins
    - [ ] Generic Webhook
- [x] Git credential helper


## Similar and related projects

### DPL from Travis CI

https://github.com/travis-ci/dpl

Provides a way to deploy artifacts to different storage providers.

While CI Kit also provides the same functionality it also goes further in assisting with the step of propagating the artifacts between pipelines
across different projects and CI environment.

Further dpl is written in Ruby while CI Kit is written in Rust. This makes it a lot simple to deploy as it is just a single static binary.
